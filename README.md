# Blocklistlist

## Active or automated

### Oliphant.Social Mastodon Blocklists

* https://writer.oliphant.social/oliphant/the-oliphant-social-blocklist
    * https://codeberg.org/oliphant/blocklists
    * https://github.com/sgrigson/oliphant

### My Fediverse blocklists - Seirdy

* https://seirdy.one/posts/2023/05/02/fediverse-blocklists/

## Active or inactive

No change log nor timestamp.

### Fediblock FYI

* https://fediblock.neocities.org/

### List of instances blocked by chaos.social

* ~~https://github.com/chaossocial/about/blob/master/blocked_instances.md~~
* https://meta.chaos.social/federation

## Inactive

### BLOCKchain

* https://github.com/dzuk-mutant/blockchain/

### FediBlock

* https://schlomp.space/tastytea/FediBlock

### FediFence

* https://github.com/FediFence/fedifence

### List of Toot Café blocked instances

* https://github.com/tootcafe/blocked-instances

### FediBlock

* https://joinfediverse.wiki/FediBlock

### Fediblock

* https://github.com/DrivetDevelopment/FediBlock

### The RapidBlock Project 

* https://rapidblock.org/
* https://social.rapidblock.org/@rapidblock
* https://github.com/rapidblock-org/rapidblock

### vulpine.club instance blocklist

* https://vulpineclub.github.io/blocklist
